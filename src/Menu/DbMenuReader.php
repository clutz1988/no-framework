<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 02:00
 */

namespace NoFramework\Menu;

use NoFramework\Cache\Cache;
use \PDO;

class DbMenuReader implements MenuReader
{
	/**
	 * @var PDO
	 */
	private $pdo;
	/**
	 * @var Cache
	 */
	private $cache;

	public function __construct(PDO $pdo, Cache $cache)
	{
		$this->pdo = $pdo;
		$this->cache = $cache;
	}

	public function readMenu()
	{
		$sql = "
			SELECT *
			FROM `menu_item`
		";
		$cacheKey = array( 'key' => md5($sql));
		if(null === ($row = $this->cache->get($cacheKey))) {
			$stmt = $this->pdo->prepare($sql);
			$stmt->execute();
			$row = $stmt->fetchAll();
			$this->cache->add($cacheKey, $row, 21600);
		}
		return $row;
	}
}