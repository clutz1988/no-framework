<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 01:30
 */

namespace NoFramework\Menu;


interface MenuReader
{
	public function readMenu();
}