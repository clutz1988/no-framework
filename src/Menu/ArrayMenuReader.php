<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 01:31
 */

namespace NoFramework\Menu;


class ArrayMenuReader implements MenuReader
{
	public function readMenu()
	{
		return array(
			array('href' => '/', 'text' => 'Homepage'),
			array('href' => '/page-one', 'text' => 'Page One')
		);
	}
}