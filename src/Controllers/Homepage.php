<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 23/10/2015
 * Time: 22:59
 */

namespace NoFramework\Controllers;

use Http\Request;
use Http\Response;
use NoFramework\Meta\MetaReader;
use NoFramework\Template\FrontendRenderer;

class Homepage
{
	/**
	 * @var Response
	 */
	private $response;
	/**
	 * @var Request
	 */
	private $request;
	/**
	 * @var FrontendRenderer
	 */
	private $renderer;
	/**
	 * @var MetaReader
	 */
	private $metaReader;

	public function __construct(Request $request, Response $response, FrontendRenderer $renderer, MetaReader $metaReader)
	{
		$this->request = $request;
		$this->response = $response;
		$this->renderer = $renderer;
		$this->metaReader = $metaReader;
	}

	public function show()
	{
		$data = array(
			'name'				=> $this->request->getParameter('name', 'stranger'),
			'execution_time'	=> sprintf("%.4f", microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]),
		);
		$data = array_merge($data, $this->metaReader->readByPageId(1));
		$html = $this->renderer->render('page/homepage.php', $data);
		$this->response->setContent($html);
	}
}