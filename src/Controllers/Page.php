<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 00:26
 */

namespace NoFramework\Controllers;

use Http\Request;
use Http\Response;
use NoFramework\Meta\MetaReader;
use NoFramework\Page\InvalidPageException;
use NoFramework\Page\PageReader;
use NoFramework\Template\FrontendRenderer;

class Page
{
	/**
	 * @var Response
	 */
	private $response;
	/**
	 * @var Request
	 */
	private $request;
	/**
	 * @var FrontendRenderer
	 */
	private $renderer;
	/**
	 * @var PageReader
	 */
	private $pageReader;
	/**
	 * @var MetaReader
	 */
	private $metaReader;

	function __construct(Response $response, Request $request, FrontendRenderer $renderer, PageReader $pageReader, MetaReader $metaReader)
	{
		$this->response = $response;
		$this->request = $request;
		$this->renderer = $renderer;
		$this->pageReader = $pageReader;
		$this->metaReader = $metaReader;
	}

	public function show()
	{
		$slug = ltrim(strtok($this->request->getUri(), '?'), '/');
		try{
			$data = $this->pageReader->readBySlug($slug);
			$data = array_merge($data, $this->metaReader->readByPageId($data['page_id']));
		} catch (InvalidPageException $e) {
			$this->response->setStatusCode(404);
			return $this->response->setContent('404 - Page not found!');
		}
		$data['execution_time'] = sprintf("%.4f", microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
		$html = $this->renderer->render('page/page.php', $data);
		return $this->response->setContent($html);
	}

}