<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 01/11/2015
 * Time: 19:12
 */

namespace NoFramework\Router;

use NoFramework\Cache\Cache;
use \PDO;

class DbRouter implements Router
{
	/**
	 * @var PDO
	 */
	private $pdo;
	/**
	 * @var Cache
	 */
	private $cache;

	public function __construct(PDO $pdo, Cache $cache)
	{
		$this->pdo = $pdo;
		$this->cache = $cache;
	}

	public function getAllRoutes()
	{
		$sql = "
			SELECT *
			FROM `routes`
		";
		$cacheKey = array( 'key' => md5($sql));
		if(null === ($return = $this->cache->get($cacheKey))) {
			$stmt = $this->pdo->prepare($sql);
			$stmt->execute();
			$rows = $stmt->fetchAll();
			$return = array();
			foreach ($rows as $row) {
				$return[] = array(
					$row['request'],
					$row['uri'],
					array(
						$row['class'],
						$row['method']
					)
				);
			}
			$this->cache->add($cacheKey, $return, 3600);
		}
		return $return;
	}
}