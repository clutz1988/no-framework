<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 01/11/2015
 * Time: 19:11
 */

namespace NoFramework\Router;


interface Router
{

	public function getAllRoutes();

}