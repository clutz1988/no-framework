<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 00:17
 */

namespace NoFramework\Template;

use Mustache_Engine;

class MustacheRenderer implements Renderer
{
	/**
	 * @var Mustache_Engine
	 */
	private $engine;

	public function __construct(Mustache_Engine $engine)
	{
		$this->engine = $engine;
	}

	public function render($template, $data = [])
	{
		return $this->engine->render($template, $data);
	}}