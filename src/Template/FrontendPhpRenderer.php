<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 04/11/2015
 * Time: 23:34
 */

namespace NoFramework\Template;


use NoFramework\Menu\MenuReader;

class FrontendPhpRenderer implements FrontendRenderer
{
	/**
	 * @var MenuReader
	 */
	private $menuReader;

	public function __construct(MenuReader $menuReader)
	{
		$this->menuReader = $menuReader;
	}

	public function render($template, $data = [])
	{
		$data['menuItems'] = $this->menuReader->readMenu();
		extract($data);
		include ROOT_DIR . '/../templates/' . $template;
	}

}