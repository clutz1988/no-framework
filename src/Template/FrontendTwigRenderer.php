<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 01:19
 */

namespace NoFramework\Template;


use NoFramework\Menu\MenuReader;

class FrontendTwigRenderer implements FrontendRenderer
{
	/**
	 * @var Renderer
	 */
	private $renderer;

	/**
	 * @var MenuReader
	 */
	private $menuReader;

	public function __construct(Renderer $renderer, MenuReader $menuReader)
	{
		$this->renderer = $renderer;
		$this->menuReader = $menuReader;
	}

	public function render($template, $data = [])
	{
		$data = array_merge($data, array(
			'menuItems' => $this->menuReader->readMenu(),
		));
		return $this->renderer->render($template, $data);
	}

}