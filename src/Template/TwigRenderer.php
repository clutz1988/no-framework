<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 01:00
 */

namespace NoFramework\Template;

use Twig_Environment;

class TwigRenderer implements Renderer
{
	/**
	 * @var Twig_Environment
	 */
	private $renderer;

	public function __construct(Twig_Environment $renderer)
	{
		$this->renderer = $renderer;
	}

	public function render($template, $data = [])
	{
		return $this->renderer->render("$template.html", $data);
	}

}