<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 21:42
 */

namespace NoFramework\Meta;


interface MetaReader
{
	public function readByPageId($page_id);
}