<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 21:42
 */

namespace NoFramework\Meta;

use NoFramework\Cache\Cache;
use \PDO;

class DbMetaReader implements MetaReader
{
	/**
	 * @var PDO
	 */
	private $pdo;

	private $cache;

	public function __construct(PDO $pdo, Cache $cache)
	{
		$this->pdo = $pdo;
		$this->cache = $cache;
	}

	public function readByPageId($page_id)
	{
		$sql = "
			SELECT `page_title`, `meta_description`
			FROM `page_meta`
			WHERE `page_id` = :pageId
		";
		$params = array(
			'pageId'	=> $page_id,
		);
		$cacheKey = array( 'key' => md5($sql . $page_id));
		if(null === ($rows = $this->cache->get($cacheKey))) {
			$stmt = $this->pdo->prepare($sql);
			$stmt->execute($params);
			$rows = $stmt->fetch();
			$this->cache->add($cacheKey, $rows, 21600);
		}
		return $rows;
	}

}