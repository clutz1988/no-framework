<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 00:39
 */

namespace NoFramework\Page;


interface PageReader
{
	public function readBySlug($slug);
}