<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 00:40
 */

namespace NoFramework\Page;


class FilePageReader implements PageReader
{
	private $pageFolder;

	public function __construct($pageFolder)
	{
		if(!is_string($pageFolder))
			throw new \InvalidArgumentException('pageFolder must be a string');

		$this->pageFolder = $pageFolder;
	}

	public function readBySlug($slug)
	{
		if(!is_string($slug))
			throw new \InvalidArgumentException('slug must be a string');

		$path = $this->pageFolder . '/' . $slug . '.md';

		if(!file_exists($path))
			throw new InvalidPageException($slug);

		return file_get_contents($path);
	}
}