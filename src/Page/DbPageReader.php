<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 17:13
 */

namespace NoFramework\Page;

use NoFramework\Cache\Cache;
use \PDO;

class DbPageReader implements PageReader
{
	/**
	 * @var PDO
	 */
	private $pdo;
	/**
	 * @var Cache
	 */
	private $cache;

	public function __construct(PDO $pdo, Cache $cache)
	{
		$this->pdo = $pdo;
		$this->cache = $cache;
	}

	public function readBySlug($slug)
	{
		$sql = "
			SELECT `page_id`, `content`, `slug`
			FROM `page`
			WHERE `slug` = :slug
		";
		$params = array(
			'slug'	=> $slug
		);
		$cacheKey = array( 'key' => md5($sql . $slug));
		if(null === ($row = $this->cache->get($cacheKey))) {
			$stmt = $this->pdo->prepare($sql);
			$stmt->execute($params);
			$row = $stmt->fetch();
			$this->cache->add($cacheKey, $row, 21600);
		}
		if(!empty($row))
			return $row;

		throw new InvalidPageException($slug);
	}

}