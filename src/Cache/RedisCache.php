<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 23:45
 */

namespace NoFramework\Cache;

use Sonata\Cache\Adapter\Cache\PRedisCache;

class RedisCache implements Cache
{
	/**
	 * @var PRedisCache
	 */
	private $redis;

	public function __construct(PRedisCache $redis)
	{
		$this->redis = $redis;
	}

	public function add($key, $data, $expires = 21600)
	{
		$this->redis->set($key, $data, $expires);
	}

	public function get($key)
	{
		return $this->redis->get($key)->getData();
	}

	public function delete($key)
	{
		$this->redis->flush($key);
	}
}