<?php
/**
 * Created by PhpStorm.
 * User: Clutz
 * Date: 24/10/2015
 * Time: 23:15
 */

namespace NoFramework\Cache;


interface Cache
{

	public function add($key, $data, $expires);

	public function get($key);

	public function delete($key);

}