<?php

$injector = new \Auryn\Injector;

$injector->alias('Http\Response', 'Http\HttpResponse');
$injector->share('Http\HttpResponse');

$injector->alias('Http\Request', 'Http\HttpRequest');
$injector->share('Http\HttpRequest');
$injector->define('Http\HttpRequest', [
	':get' => $_GET,
	':post' => $_POST,
	':cookies' => $_COOKIE,
	':files' => $_FILES,
	':server' => $_SERVER,
]);

$injector->define('Sonata\Cache\Adapter\Cache\PRedisCache', [
	':host' => '127.0.0.1',
	':port' => 6379,
	':database' => 42,
]);

$injector->alias('NoFramework\Cache\Cache', 'NoFramework\Cache\RedisCache');
$injector->share('NoFramework\Cache\RedisCache');

$injector->alias('NoFramework\Router\Router', 'NoFramework\Router\DbRouter');
$injector->share('NoFramework\Router\DbRouter');



$injector->alias('NoFramework\Template\FrontendRenderer', 'NoFramework\Template\FrontendPhpRenderer');

$injector->alias('NoFramework\Page\PageReader', 'NoFramework\Page\DbPageReader');
$injector->share('NoFramework\Page\DbPageReader');

$injector->alias('NoFramework\Menu\MenuReader', 'NoFramework\Menu\DbMenuReader');
$injector->share('NoFramework\Menu\DbMenuReader');

$injector->alias('NoFramework\Meta\MetaReader', 'NoFramework\Meta\DbMetaReader');
$injector->share('NoFramework\Meta\DbMetaReader');

$injector->share('PDO');
$injector->define('PDO', [
	':dsn' => 'mysql:dbname=noframework;host=localhost',
	':username' => 'noframework',
	':passwd' => 'nillframework'
]);

return $injector;