<?php
return [
	['GET', '/', ['NoFramework\Controllers\Homepage', 'show']],
	['GET', '/{slug}', ['NoFramework\Controllers\Page', 'show']],
];