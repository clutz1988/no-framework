<div id="menu">
	<div class="pure-menu">
		<ul class="pure-menu-list">
			<?php foreach($menuItems as $item) { ?>
			<li class="pure-menu-item"><a href="<?php echo $item['href']; ?>" class="pure-menu-link"><?php echo $item['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>